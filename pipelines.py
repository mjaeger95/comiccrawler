from scrapy import Request
from scrapy.pipelines.images import ImagesPipeline


# downloads an image to the specified path within global download path
class CustomImagePipeline(ImagesPipeline):

	def get_media_requests(self, item, info):
		return Request(item["url"], meta={'name': item["name"], "path": item["path"]})

	def file_path(self, request, response=None, info=None):
		return "{path}/{name}.jpg".format(**request.meta)
