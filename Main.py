import json

from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from spiders import ComicSpider


# filter for debug purposes
active_comics = None  # DoA.DumbingOfAge
dynamic_spiders = True  # defined in spiders.JSON
static_spiders = True  # defined in .py Files


# TODO Feed into RSS web server

def main():
	# load settings
	process = CrawlerProcess(get_project_settings())

	# load dynamic spiders
	if dynamic_spiders:
		# open comic list
		with open('spiders.json') as f: my_comics = json.load(f)
		# update list
		for comic in my_comics: my_comics[comic]["name"] = comic
		for name, attributes in my_comics.items():
			if not active_comics or name is active_comics or name in active_comics:
				process.crawl(type(name, (ComicSpider.ComicSpider,), my_comics[name]))

	# load static spiders
	if static_spiders:
		for spider in process.spider_loader.list():
			if not active_comics or spider.name is active_comics or spider.name in active_comics:
				process.crawl(spider)

	# start crawling
	process.start()


if __name__ == "__main__":
	main()
