from spiders.ComicSpider import ComicSpider


class Godslave(ComicSpider):
	name = "Godslave"

	# index_url = "http://www.godslavecomic.com/comic/archive"
	# index_selector = "select > option::attr(value)"
	start_url = "http://www.godslavecomic.com/comic/page-1"
	next_selector = "a.cc-next"

	image_selector = "img#cc-comic"
	use_count = True
	title_selector = "img#cc-comic::attr(title)"
