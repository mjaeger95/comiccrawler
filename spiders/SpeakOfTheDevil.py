from spiders.ComicSpider import ComicSpider


class SpeakOfTheDevil(ComicSpider):
	name = "SpeakOfTheDevil"

	# index_url = "http://speakdevil.com/comic/archive"
	# index_selector = "select > option::attr(value)"
	start_url = "http://speakdevil.com/comic/chapter-1-cover"
	next_selector = "a.cc-next"

	image_selector = "img#cc-comic"
	use_count = True
	title_selector = "div.cc-newsheader::text"
