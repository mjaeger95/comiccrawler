from spiders.ComicSpider import ComicSpider


class Wildlife(ComicSpider):
	name = "Wildlife"

	# index_url = "http://www.wildelifecomic.com/comic/archive"
	# index_selector = "select > option::attr(value)"
	start_url = "http://www.wildelifecomic.com/comic/1"
	next_selector = "a.cc-next"

	image_selector = "div#cc-comicbody > a > img"
	use_count = True
	title_selector = "div.cc-newsheader::text"
