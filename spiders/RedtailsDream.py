from spiders.ComicSpider import ComicSpider


class RedtailsDream(ComicSpider):
	name = "RedtailsDream"

	index_url = "http://www.minnasundberg.fi/comicindex.php"
	index_selector = "a.text::attr(href)"
	# start_url = "http://www.minnasundberg.fi/comicindex.php"
	next_selector = "td > a[href^=page]:nth-of-type(1)"

	image_selector = "div#page > img"
	title_selector = "p.num::text"


