from spiders.ComicSpider import ComicSpider


class Tellurion(ComicSpider):
	name = "Tellurion"

	# archive-page exists but is script-based
	start_url = "https://tellurion.ca/projects/yqNgO?album_id=77988"
	next_selector = "a.project-page-next"

	image_selector = "img.img-fluid.constrained"
	title_selector = "h1.text-center::text"
