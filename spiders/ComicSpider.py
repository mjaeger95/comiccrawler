from urllib.parse import urlparse
from scrapy import Spider, Request
from items import ComicPage


# for lack of python being object oriented this abstract base class is a regular spider that doesn't work properly.
# All None-methods and fields must be overridden by subclass
class ComicSpider(Spider):
	name = None

	# navigation
	index_url = None
	index_selector = None
	start_url = None  # can be str or list of str
	next_selector = None

	# extraction
	image_selector = None

	chapter_number = None
	chapter_selector = None  # will use name of the class if not specified

	use_count = False
	title_selector = None  # will use basename of url if not specified
	include_base_name = False  # see _name()
	count = 0

	# avoid
	def _image(self, response):
		"""
		Extracts the image url from a comic page.
		Shouldn't have to be overwritten except in edge cases eg. javascript-heavy pages
		:param response:
		:return: url as str or None if selector is invalid
		"""
		image = response.css(self.image_selector).get()
		if image:
			return response.urljoin(image.strip())
		else:
			self.logger.warning(
				"Selector '{}' does not yield anything on url '{}'".format(self.image_selector, response.url))
			return None

	def _path(self, response):
		"""
		extracts the path for where to save the extracted image
		:param response:
		:return: a relative path as str
		"""
		path = [self.name, '/']
		if self.chapter_number:
			cn = response.css(self.chapter_number).get()
			if cn:
				path.append(cn.strip())
				if self.chapter_selector:
					path.append(' ')
		if self.chapter_selector:
			cn = response.css(self.chapter_selector).get()
			if cn:
				path.append(cn.strip())
		return ''.join(path)

	def _name(self, response):
		"""
		Extracts the file name in which to save the extracted image.
		Shouldn't have to be overwritten except in edge cases
		:param response:
		:return: file name as str
		"""
		name = []
		if self.use_count:
			name.append(str(self.count))
		if self.include_base_name or not self.use_count and not self.title_selector:
			base_name = urlparse(response.request.url).path.split('/')
			name.append(base_name[-1] if base_name[-1] is not '' else base_name[-2])
		if self.title_selector:
			title = response.css(self.title_selector).get()
			if title: name.append(title.strip().replace(':', ''))
		return ' '.join(name)

	# don't override
	def start_requests(self):
		"""
		Starts requesting comics. Begins with the index if provided and numbering is not enabled.
		Otherwise accepts url(s) as str or list of str
		:return:
		"""
		# initialization
		if self.image_selector: self.image_selector += "::attr(src)"
		else: self.logger.error("image_selector not specified!")
		if self.next_selector: self.next_selector += "::attr(href)"
		# index / archive page handling
		if self.index_url and self.index_selector:
			if not self.use_count: yield Request(self.index_url, self.parse_index)
			else: self.logger.warning("not using provided index because numbering is required")
		# multiple starting points
		if isinstance(self.start_url, list):
			for u in self.start_url: yield Request(u, self.parse)
		# single starting point (default scenario)
		elif self.start_url and isinstance(self.start_url, str): yield Request(self.start_url, self.parse)

	# TODO description texts, Alt Texts
	def parse(self, response):
		"""
		extracts a single comic page and a link to the next one
		:param response:
		:return: yields one ComicPage object and one parse Request
		"""
		self.count += 1
		page = ComicPage()
		page["url"] = self._image(response)
		page["name"] = self._name(response)
		page["path"] = self._path(response)
		if all(page.values()):
			self.logger.info("found image {path}/{name}".format(**page))
			yield page

		url = response.css(self.next_selector).get()
		if url:
			url = response.urljoin(url)
			self.logger.info("continue with '{}'".format(url))
			yield Request(url, callback=self.parse)

	def parse_index(self, response):
		"""
		use index_url and index_selector to extract links to pages
		:param response:
		:return: yields parse requests
		"""
		urls = response.css(self.index_selector).getall()
		base_url = urlparse(response.request.url)
		base_url = base_url.scheme + "://" + base_url.netloc + '/'
		for url in urls:
			url = base_url + url
			yield Request(url, callback=self.parse)
		self.logger.info("reading {} urls from index".format(len(urls)))
