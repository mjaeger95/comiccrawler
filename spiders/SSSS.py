from urllib.parse import urlparse
from spiders.ComicSpider import ComicSpider


class SSSS(ComicSpider):
	name = "SSSS"

	index_url = "http://www.sssscomic.com/index.php?id=archive"
	index_selector = "div.archivediv > a::attr(href)"
	# start_url = ["http://sssscomic.com/comic.php?page=1", "http://sssscomic.com/comic.php?page=2"]
	next_selector = 'div.comicnav a:nth-of-type(3)'

	image_selector = "img.comicnormal"
	title_selector = "div.pagenum > p::text"

	def _path(self, response):
		return "SSSS/adventure 1" if urlparse(response.request.url).path[1:-4] != "comic2" else "SSSS/adventure 2"
