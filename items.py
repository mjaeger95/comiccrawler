from scrapy import Field, Item


class ComicPage(Item):
	url = Field()
	name = Field()
	path = Field()
