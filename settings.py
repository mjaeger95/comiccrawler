BOT_NAME = 'ComicCrawler'

SPIDER_MODULES = ['spiders']
NEWSPIDER_MODULE = 'spiders'
ITEM_PIPELINES = {'pipelines.CustomImagePipeline': 1}
IMAGES_STORE = 'C:/ComicCrawler/'  # adjust at will
MEDIA_ALLOW_REDIRECTS = True  # for adding / removing www.

LOG_LEVEL = 'INFO'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = BOT_NAME

# Obey robots.txt rules
ROBOTSTXT_OBEY = True
# CLOSESPIDER_PAGECOUNT = 5

FEED_EXPORTERS = {'jsonlines': 'scrapy.contrib.exporter.JsonLinesItemExporter'}
FEED_URI = "file:///C:/ComicCrawler/results.json"
FEED_FORMAT = "jsonlines"

# AUTOTHROTTLE_ENABLED = True
# AUTOTHROTTLE_TARGET_CONCURRENCY = 10
