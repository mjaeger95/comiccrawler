# Comic Crawler

This project uses scrapy to batch-download webcomics and other formats. 

Spiders for additional comics are added irregularly. Feel free to contribute.

Intended future features include dumping the collected information into a JSON or CSV file and eventually use these to host an RSS provider.